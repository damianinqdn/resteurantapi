using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResteurantAPI.Entities;

namespace ResteurantAPI
{
    
    public class RestaurantSeeder
    {
        private readonly RestaurantDbContext _context;

        public RestaurantSeeder(RestaurantDbContext context)
        {
            _context = context;
        }
        
        public void Seed()
        {
            if(_context.Database.CanConnect())
            {
                if(!_context.Restaurants.Any())
                {
                    var restaurants = GetRestaurants();
                    _context.Restaurants.AddRange(restaurants);
                    _context.SaveChanges();
                }
            }
        }
    // IEnumerable - read only access collection
        private IEnumerable<Restaurant> GetRestaurants()
        {
            var restaurants = new List<Restaurant>()
            {
                new Restaurant()
                {
                    Name = "KFC",
                    Category = "Fast Food",
                    Description = "American chicken",
                    ContactEmail = "kfc@gmail.com",
                    HasDelivery = true,
                    Dishes = new List<Dish>()
                    {
                        new Dish()
                        {
                            Name = "Nashville Hot Chicken",
                            Price = 10.50M,
                        },

                        new Dish()
                        {
                            Name = "Chicken Nuggets",
                            Price = 17.20M,
                        }
                    },
                    Address = new Address()
                    {
                        City = "Sławków",
                        Street = "Krakowska",
                        PostalCode = "41-260"
                    }
                },
                new Restaurant()
                {
                    Name = "McDonald's",
                    Category = "Fast Food",
                    Description = "Beast chicken in the world",
                    ContactEmail = "mcdonalds@interia.pl",
                    HasDelivery = false,
                    Dishes = new List<Dish>()
                    {
                        new Dish()
                        {
                            Name = "Mc Spicy Chicken",
                            Price = 21.99M
                        },

                        new Dish()
                        {
                            Name = "Chicken Nuggets",
                            Price = 15.20M
                        }
                    },
                    Address = new Address()
                    {
                        City = "Kraków",
                        Street = "Sławkowska",
                        PostalCode = "32-250"
                    }
                }
            };
            return restaurants;
        }
    }
}