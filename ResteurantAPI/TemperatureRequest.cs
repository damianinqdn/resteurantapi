using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResteurantAPI
{
    /* Object for request (DTO) */
    public class TemperatureRequest
    {
        public int count {get; set;}
        public int minTemperature {get; set;}
        public int maxTemperature {get; set;}
    }
}