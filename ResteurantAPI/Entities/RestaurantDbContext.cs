using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

// dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version '5.0.1'
// dotnet add package Microsoft.EntityFrameworkCore.Tools --version '5.0.1'

/* create migration and updata database (mssql server)
dotnet tool install --global dotnet-ef --version '5.0.1'
dotnet ef migrations add Init
dotnet ef database update
*/


namespace ResteurantAPI.Entities
{
    public class RestaurantDbContext : DbContext
    {
        private string _connectionString =
            // @"Server=(localdb)\MSSQLLocalDB;Database=RestaurantDb;Trusted_Connection=True;";
            "Server=127.0.0.1,1433;database=RestaurantDb;UID=sa;PWD=Alicjawwa112";
        public DbSet<Restaurant> Restaurants { get; set;}
        public DbSet<Address> Addresses { get; set;}
        public DbSet<Dish> Dishes { get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Restaurant>()
                .Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(25);
            modelBuilder.Entity<Dish>()
                .Property(d => d.Name)
                .IsRequired();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}