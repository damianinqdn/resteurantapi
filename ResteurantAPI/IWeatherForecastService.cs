using System.Collections.Generic;

namespace ResteurantAPI
{
    public interface IWeatherForecastService
    {
        IEnumerable<WeatherForecast> Get();
        IEnumerable<WeatherForecast> GetByParams(int count, int minTemperature, int maxTemperature);
    }
}