using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// adding AutoMapper
using AutoMapper;
using ResteurantAPI.Entities;
using ResteurantAPI.Models;

namespace ResteurantAPI
{
    public class ResaturantMappingProfile : Profile
    {
        public ResaturantMappingProfile()
        {
            // CreateMap <from, to> 
            CreateMap<Restaurant, RestaurantDto>()
                // for not easy conwersion, (Entity does not match database)
                .ForMember(m => m.City, c=> c.MapFrom(s => s.Address.City))
                .ForMember(m => m.Street, c=> c.MapFrom(s => s.Address.Street))
                .ForMember(m => m.PostalCode, c=> c.MapFrom(s => s.Address.PostalCode));

            CreateMap<Dish, DishDto>();
        
            CreateMap<CreateRestaurantDto, Restaurant>()
                .ForMember(r => r.Address, c => c.MapFrom(dto => new Address()
                {
                    City = dto.City,
                    Street = dto.Street,
                    PostalCode = dto.PostalCode
                }));
        }
    }
}