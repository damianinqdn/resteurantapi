﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ResteurantAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        
        private readonly IWeatherForecastService _service;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IWeatherForecastService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var results = _service.Get();
            return results;
        }

        [HttpPost("generate")]
        public ActionResult<IEnumerable<WeatherForecast>> GetByParams([FromBody]TemperatureRequest request)
        {
            if(request.count < 0 || request.minTemperature >= request.maxTemperature)
            {
                return BadRequest();
            }
            var results = _service.GetByParams(request.count, request.minTemperature, request.maxTemperature);
            return Ok(results);
        }

        [HttpGet]
        // [HttpGet("currentDay")]
        [Route("currentDay/{max}")]
        public IEnumerable<WeatherForecast> Get2([FromQuery]int take, [FromRoute]int max)
        {
            var results = _service.Get();
            return results;
        }

        [HttpPost]
        public ActionResult<string> Hello([FromBody]string name)
        {
            HttpContext.Response.StatusCode = 401;
            return $"Hello {name}";
        }
    }
}
