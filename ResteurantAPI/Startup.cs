using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ResteurantAPI.Entities;

namespace ResteurantAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // Dependency injection, user authorization
        public void ConfigureServices(IServiceCollection services)
        {
            /* Registration methods:
            1. Singleton - creates only once for app (1 instance from start to close app)
            2. Scoped - creates new object for each client response (1 response - 1 instance)
            3. Transient - creates new object each time the constructor is called (every query - new object)*/ 

            // Method takes 2 generics, <1> - abstraction on witch we want to install specific implementation - <2>
            services.AddTransient<IWeatherForecastService, WeatherForecastService>();
            services.AddControllers();
            // add context for Seeder
            services.AddDbContext<RestaurantDbContext>();
            services.AddScoped<RestaurantSeeder>();
            // source for AutoMapper
            services.AddAutoMapper(this.GetType().Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // Configures flow methods before response
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, RestaurantSeeder seeder)
        {
            // database SEED
            seeder.Seed();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
